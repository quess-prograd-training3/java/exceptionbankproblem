package com.example;

public class MainBank {
    public static void main(String[] args) {
        try {
            SBIBank sbiBank = new SBIBank();
            sbiBank.initializeInputs();
            sbiBank.withdraw();
        }
        catch (InsufficientBalanceException insufficientBalanceException){
            System.out.println(insufficientBalanceException.getMessage());
        }
    }
}
