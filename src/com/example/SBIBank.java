package com.example;

import java.util.Scanner;

public class SBIBank {
    private static final int minimumBalance=1000;
    int balanceAmount;
    int withDrawAmount;

    public void initializeInputs(){
        Scanner scanner=new Scanner(System.in);
        System.out.println("Enter BalanceAmount:- ");
        balanceAmount=scanner.nextInt();
        System.out.println("Enter WithDrawAmount:- ");
        withDrawAmount=scanner.nextInt();
    }

    public void withdraw() throws InsufficientBalanceException {
        if (balanceAmount - withDrawAmount < minimumBalance) {
            throw new InsufficientBalanceException("Cannot withdraw amount as it will cause balance to go below minimum balance of 1000.");
        }
        else {
            balanceAmount -= withDrawAmount;
            System.out.println(balanceAmount);
        }
    }

}
